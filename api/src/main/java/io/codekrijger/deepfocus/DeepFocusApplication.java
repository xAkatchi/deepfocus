package io.codekrijger.deepfocus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DeepFocusApplication {

    public static void main(String[] args) {
        SpringApplication.run(DeepFocusApplication.class, args);
    }

}
