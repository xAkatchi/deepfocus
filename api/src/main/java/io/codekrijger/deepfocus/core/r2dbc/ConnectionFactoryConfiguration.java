package io.codekrijger.deepfocus.core.r2dbc;

import io.r2dbc.postgresql.PostgresqlConnectionConfiguration;
import io.r2dbc.postgresql.PostgresqlConnectionFactory;
import io.r2dbc.spi.ConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.URI;

@Configuration
public class ConnectionFactoryConfiguration {

    @Bean
    public ConnectionFactory connectionFactory(
            @Value("${spring.datasource.url}") String url,
            @Value("${spring.datasource.username}") String username,
            @Value("${spring.datasource.password}") String password
    ) {
        // Parse the JDBC url to obtain the information, see: https://stackoverflow.com/a/9287197
        String cleanURI = url.substring(5);
        URI uri = URI.create(cleanURI);

        PostgresqlConnectionConfiguration config = PostgresqlConnectionConfiguration.builder()
                .host(uri.getHost())
                .port(uri.getPort())
                .database(uri.getPath())
                .username(username)
                .password(password)
                .build();

        return new PostgresqlConnectionFactory(config);
    }
}
